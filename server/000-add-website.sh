#!/bin/bash

# make sure we have permissions
if [ "$(whoami)" != "root" ]; then
        echo "Root privileges are required to run this, try running with sudo..."
        exit 2
fi

printf "\n\nMake sure you have pulled the latest version of the server admin scripts before proceeding.\n"
printf "Run 'update-server-admin-scripts.sh'\n"
printf "Would you like to proceed? (y/n) "
read answer

if [ "$answer" == "n" ]; then
  exit 1
else
  answer=''
fi

printf "\n\nThe Following scripts will be run in the order listed: \n"
printf "1. add-shell-user.sh\n"
printf "2. add-environment-sync-script.sh"
printf "3. add-database.sh (optional, you will be given a choice)\n"
printf "4. add-apache-virtualhost.sh\n"
printf "5. add-ssl.sh\n"

printf "\n\nAre you ready to proceed? (y/n) "
read answer

if [ "$answer" == "n" ]; then
  exit 1
fi

printf "got here.\n\n"
