#!/bin/bash

# make sure you are running as sudo
if [ "$(whoami)" != "root" ]; then
	echo "Root privileges are required to run this, try running with sudo..."
	exit 2
fi

current_directory="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
vhosts_path="/etc/apache2/sites-available/"
vhost_skeleton_path="$current_directory/vhost.skeleton.conf"
web_root="/home/"


printf "\n\nWhat domain are you wanting to add (omit the www, but staging.example.com is okay): "
read site_url


if [ -z "$site_url" ]
then
      echo "No Domain Name was entered. Exiting Now!"
			exit 1;
fi

printf "\n\nWhere is the webroot going to be (DO NOT enter /home/ just shelluser/craft/web/): "
read relative_doc_root

if [ -z "$relative_doc_root" ]
then
      echo "No webroot was entered. Exiting Now!"
			exit 1;
fi

# construct absolute path
absolute_doc_root=$web_root$relative_doc_root

# double check it is correct
printf "\n\nDoes this path look correct? \n"
printf "$absolute_doc_root\n"
printf "y/n: "
read path_check

# exit if the user typed in n
if [ "$path_check" -eq "n" ]
then
	printf "\n\nPath not correct. exiting.\n\n"
	exit 1;
fi

# update vhost
vhost=`cat "$vhost_skeleton_path"`
vhost=${vhost//@site_url@/$site_url}
vhost=${vhost//@site_docroot@/$absolute_doc_root}

`touch $vhosts_path$site_url.conf`
echo "$vhost" > "$vhosts_path$site_url.conf"
echo "Updated vhosts in Apache config"

# restart apache
echo "Enabling site in Apache..."
echo `a2ensite $site_url`

echo "Restarting Apache..."
echo `/etc/init.d/apache2 restart`

echo "Process complete, check out the new site at http://$site_url"

exit 0
