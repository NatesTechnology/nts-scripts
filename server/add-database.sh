#!/bin/bash

# make sure we have permissions
if [ "$(whoami)" != "root" ]; then
        echo "Root privileges are required to run this, try running with sudo..."
        exit 2
fi

# Make the screen pretty
printf "\n\n"
printf "************************** Existing Databases ***************************\n\n"

# show the list of existing databases
mysqlshow --defaults-extra-file='/root/.mysql.cnf'

# get the name of the new db
printf "\n\nWhat name for your new database: "
read db_name

# check to see if this database name already exists
RESULT=`mysqlshow --defaults-extra-file='/root/.mysql.cnf' $db_name| grep -v Wildcard | grep -o $db_name`
if [ "$RESULT" == "$db_name" ]; then
    echo This Database name already exists.
    exit 1
fi


# Make the screen pretty
printf "\n\n"
printf "************************** Existing Users ***************************\n\n"

# Show a list of existing database users
mysql --defaults-extra-file='/root/.mysql.cnf' -sse "SELECT User FROM mysql.user"

# get the username for this database
printf "\n\nWhat username would you like to use for this database: "
read db_username

# check to see if the username already exists
RESULT="$(mysql --defaults-extra-file='/root/.mysql.cnf' -sse "SELECT EXISTS(SELECT 1 FROM mysql.user WHERE user = '$db_username')")"

if [ "$RESULT" = 1 ]; then
  echo "Username already exists.  Exiting.  Database and User not created."
fi

# get password for database user
printf "\n\nWhat password do you want for this database user: "
read -s db_user_password


# mysql commands in variables
CREATE_DB_CMD="CREATE DATABASE ${db_name}"
CREATE_USER_CMD="CREATE USER ${db_username}@localhost IDENTIFIED BY '${db_user_password}';"
DB_USER_PERMISSIONS_LIST="SELECT, INSERT, DELETE, UPDATE, CREATE, ALTER, INDEX, DROP, REFERENCES, LOCK TABLES"
GRANT_USER_PERMISSIONS_CMD="GRANT ${DB_USER_PERMISSIONS_LIST} ON ${db_name}.* TO ${db_username}@localhost;"
FLUSH_PRIVILEGES_CMD="FLUSH PRIVILEGES;"

# Run the mysql commands
mysql --defaults-extra-file='/root/.mysql.cnf' -sse "$CREATE_DB_CMD"
mysql --defaults-extra-file='/root/.mysql.cnf' -sse "$CREATE_USER_CMD"
mysql --defaults-extra-file='/root/.mysql.cnf' -sse "$GRANT_USER_PERMISSIONS_CMD"
mysql --defaults-extra-file='/root/.mysql.cnf' -sse "$FLUSH_PRIVILEGES_CMD"

printf "\n\n"
