#!/bin/bash

# make sure we have permissions
if [ "$(whoami)" != "root" ]; then
        echo "Root privileges are required to run this, try running with sudo..."
        exit 2
fi

# get the shell user
printf "\n\nWhat is the shell username where the environment sync script will be copied to (just the username)? "
read shell_username

# make sure the user and home directory exists
if [ -d "/home/$shell_username/" ]; then
  # copy scripts over and change owners
  printf "\n\nCopying Scripts"
  cp ./environment-sync.sh /home/$shell_username/environment-sync.sh
  chown $shell_username:www-data /home/$shell_username/environment-sync.sh

  if [ -f "/home/$shell_username/.env-sync.cnf" ]; then
    timestamp=`date +'%Y-%m-%d___%H-%M-%S'`
    printf "\n\nA config file already exists for this shell user.  The current config will be moved \nto .env-sync.$timestamp.cnf"
    mv /home/$shell_username/.env-sync.cnf /home/$shell_username/.env-sync.$timestamp.cnf
  fi
  cp ./sample.env-sync.cnf /home/$shell_username/.env-sync.cnf
  chown $shell_username:www-data /home/$shell_username/.env-sync.cnf
else
  printf "\n\n/home/$shell_username/ doesn't exist.  Exiting.\n\n"
  exit 2
fi

# Instruct and exit
printf "\n\nDone Copying. \n\n"
printf "Make sure to:\n"
printf "1. Edit the /home/$shell_username/.env-sync.cnf\n"
printf "2. If environment-sync.sh is ssh'ing into another server to sync the\nassets, make sure to login as the shell user and ssh-copy-id to the remote server"

printf "\n\n"
