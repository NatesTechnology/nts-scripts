#!/bin/bash

# make sure we have permissions
if [ "$(whoami)" != "root" ]; then
        echo "Root privileges are required to run this, try running with sudo..."
        exit 2
fi

printf "Enter the username to add: "
read new_user

printf "\n\n"

# add the new user
adduser --ingroup www-data --shell /bin/bash $new_user

printf "\n\n"

# add new user to the right groups
usermod -a -G ssh,mysql $new_user

printf "\n\n"

printf "1. Now ssh-copy-id for your new shell user on your local computer.\n"
printf "2. Then, ssh in and ssh-keygen for the shell user.\n"
printf "3. Finally, cat ~/.ssh/id_rsa.pub and copy that key into the the repo keys."
