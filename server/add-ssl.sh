#!/bin/bash

# make sure we have permissions
if [ "$(whoami)" != "root" ]; then
        echo "Root privileges are required to run this, try running with sudo..."
        exit 2
fi


# run certbot
certbot

