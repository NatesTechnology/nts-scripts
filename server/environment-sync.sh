#!/bin/bash


# See if the environmental config file is present
if [ -f .env-sync.cnf ]; then
  printf "\n\nConfig File Found...Proceeding"
  source .env-sync.cnf
else
  printf "\n\nConfig File '.env-sync.cnf' not found.  Exiting.\n\n"
  exit 1
fi


# See if the git repository exists yet.
(
  if [ -d ${GIT_REPO_PATH}.git ]; then
    printf "\n\nThe GIT repo exists. Pulling updates from git branch.\n\n"
    cd $GIT_REPO_PATH
    $GIT_PULL_CMD
  else
    printf "\n\nThe GIT repo hasn't been downloaded yet.  Cloning.\n\n"
    $GIT_CLONE_CMD
    cd $GIT_REPO_PATH
    printf "\n\nPulling git branch.\n\n"
    $GIT_PULL_CMD
  fi
)

# Check to see if composer install needs to be run
if [ $RUN_COMPOSER == "YES" ]; then
  (
    printf "\n\nRunning composer install\n\n"
    cd $COMPOSER_JSON_PATH
    composer install
  )

fi

# Only want to do the next two things if the server is not production
if [ $ENVIRONMENT == "staging" ] || [ $ENVIRONMENT == "development" ]; then

  if [ $SYNC_ASSETS == "YES" ]; then

    # Pull down assets from prodution server
    printf "Getting assets from the production server.\n\n"
    rsync -avzh ${SSH_USER}@${SSH_HOST}:${REMOTE_ASSET_PATH} $LOCAL_ASSET_PATH

  fi # if SYNC_ASSETS

  printf "\n\n"  

  # GET THE DB DUMP FROM THE PRODUCTION SERVER
  printf "Getting remote database dump.\n\n"
  ssh ${SSH_USER}@${SSH_HOST} "mysqldump --defaults-extra-file='~/.mysql.cnf' --user=${REMOTE_DB_USER} --host=${REMOTE_DB_HOST} --port=${REMOTE_DB_PORT} $REMOTE_DB_NAME" > $DB_DUMP_FILENAME

  printf "\n\n"

  # Import the production database dump into the local database
  printf "Importing remote database dump into local database.\n\n"
  if [ -f $DB_DUMP_FILENAME ]; then
    $LOCAL_DB_PATH --defaults-extra-file='~/.mysql.cnf' --host=$LOCAL_DB_HOST -u${LOCAL_DB_USER} $LOCAL_DB_NAME < $DB_DUMP_FILENAME
    rm $DB_DUMP_FILENAME
  else
    printf "The DB Dump File does not exist.  Database not updated.\n\n"
  fi

fi

printf "\n\n"



if [ $CRAFT_ENV_FILE_CHECK == "YES" ]; then
  if [ ! -f ${GIT_REPO_PATH}craft/.env ]; then
    printf "\n\n******** Warning! Don't forget to setup your craft cms .env file in ${GIT_REPO_PATH}craft\n\n"
  fi

  # make sure /craft/storage is writable
  chmod 775 ${GIT_REPO_PATH}craft/storage

  # make sure cp resources is writable
  chmod 775 ${GIT_REPO_PATH}craft/web/cpresources

fi
