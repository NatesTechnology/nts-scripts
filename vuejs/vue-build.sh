#!/bin/bash

BUILD_DESTINATION_PATH=""


if [ "$BUILD_DESTINATION_PATH" == "" ]; then
  printf "\n\nBuild Destination path is empty.  Exiting.\n\n"
  exit 2
fi


yarn run build

# rename build files and move them
rm ${BUILD_DESTINATION_PATH}css/*.*
mv ./dist/css ${BUILD_DESTINATION_PATH}

rm ${BUILD_DESTINATION_PATH}js/*.*
mv ./dist/js ${BUILD_DESTINATION_PATH}

rm ./dist/fav*.*
rm ./dist/inde*.*
